#include <iostream>
#include "combotree.h"

#define debug(x) std::cout << #x << ": " << x << "\n"

using namespace std;


ostream& operator<<(ostream& os, list<string> combinations)
{
    for(const string& combo : combinations)
        cout << combo << "\n";
}

int main()
{
    Node* root = new Node;

    string colors = "123";
    int depth = 3;
    ComboTree tree(root, colors, depth);

    list<string> combos = tree.getCombinations();

    cout << combos;
    debug(combos.size());


    delete root;
    return 0;
}
